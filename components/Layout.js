import Navbar from './Navbar';
import Footer from './Footer';

const Layout = ({children }) => {
  return (
    <>
    <Navbar></Navbar>
        <main className='min-h-screen pb-14'>{children}</main>
    <Footer></Footer>
    </>
  )
}

export default Layout
