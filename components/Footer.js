import styles from "../styles/footer.module.css"

const Footer = () => {
  return (
    <footer id={styles.footer} className="pl-5 md:pl-0">
        <div className="container mx-auto">
          <div className={styles.footer_copyright}>
            <span className="">Copyright © Sahaware Asessment 2021</span>
          </div>
        </div>
      </footer>
  )
}

export default Footer
