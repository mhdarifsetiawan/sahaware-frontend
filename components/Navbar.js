import Image from "next/image";
import Link from "next/link";
import logo from "../public/logo.svg"
import { useEffect, useState } from "react";

const Navbar = () => {

  const [ showModal, setShowModal ] = useState(false);
  const [ showModalReg, setShowModalReg ] = useState(false);

    const handleLogin = () => {
      const modal = document.getElementById("myModal");
      modal.classList.toggle("hidden");
      setShowModal(true);
    }

    if (showModal == true) {
      window.onclick = function(e) {
        const modal = document.getElementById("myModal");
        if (e.target == modal) {
          modal.classList.toggle("hidden");
          setShowModal(false);
        }
      }
    }

    useEffect(() => {
    },[showModal])

    useEffect(() => {
      const regisBtn = document.getElementById("regisBtn");
      const modalLogin = document.getElementById("myModal");
      regisBtn.onclick = function() {
        const modalRegis = document.getElementById("myModalRegistration")
        modalLogin.classList.toggle("hidden");
        setShowModal(false)
        modalRegis.classList.toggle("hidden");
        setShowModalReg(true)
      }
      if (showModalReg) {
        window.onclick = function(e) {
          const modalReg = document.getElementById("myModalRegistration");
          if (e.target == modalReg) {
            modalReg.classList.toggle("hidden");
            setShowModalReg(false);
          }
        }
      }
    },[showModalReg])
    
    

    function handleClickHamburger() {
        const hamburger = document.getElementById("hamburger");
        const navMenu = document.getElementById("navMenu");
        hamburger.classList.toggle("hamburgerActive");
        navMenu.classList.toggle("hidden");
        navMenu.classList.toggle("dark:bg-dark");
      }
  return (
    
    // <div className="flex items-center justify-between relative">
    <>
    <header className="bg-transparent top-0 left-0 w-full flex items-center z-10 text-dark dark:text-white border border-b-2 border-l-neutral-600">
      <div className="container mx-auto">
        <div className="flex items-center relative">
      <div className="px-4">
        <Link
          href="/"
          className="font-bold text-lg text-primary flex items-center py-6"
        >
          <div>
            <Image src={logo} width="249px" alt="logo"></Image>
          </div>
        </Link>
      </div>
      <div className="flex items-center px-4 w-full">
        <button
          id="hamburger"
          name="hamburger"
          className="block absolute right-4 lg:hidden"
          onClick={handleClickHamburger}
        >
          <span className="hamburgerLine transition duration-300 ease-in-out origin-top-left"></span>
          <span className="hamburgerLine"></span>
          <span className="hamburgerLine transition duration-300 ease-in-out origin-bottom-left"></span>
        </button>
        <nav
          id="navMenu"
          className="hidden absolute py-5 bg-white shadow-lg w-full right-0 top-full lg:block lg:static lg:bg-transparent lg:max-w-full lg:shadow-none lg:rounded-none"
        >
          <ul className="block lg:flex w-full">
            <li className="group flex justify-center">
              <Link
                href="/"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Home
              </Link>
            </li>
            <li className="group flex justify-center">
              <Link
                href="/article"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Article
              </Link>
            </li>
            <li className="group flex justify-center">
              <Link
                href="/article/create"
                className="text-base text-dark dark:text-white py-2 mx-8 flex group-hover:text-primary"
                scroll={false}
              >
                Create
              </Link>
            </li>
            <li className="group flex w-full justify-center md:justify-end">
              <button onClick={handleLogin}>Login</button>
            </li>
          </ul>
        </nav>
      </div>
        </div>
      </div>
    </header>
    </>
  );
};

export default Navbar;
