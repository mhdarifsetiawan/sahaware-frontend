
const Login = () => {

  return (
        <div id="myModal" className="bg-black bg-opacity-70 text-black modal hidden fixed z-10 pt-24 left-0 top-0 w-full h-full overflow-auto">
            <div className="w-4/5 max-w-xl bg-white m-auto p-5 border modal-content">
                <div className="px-8">
                    <span className="text-4xl font-bold text-black">Login</span>
                </div>
                <div className="px-8 text-base font-normal mt-2">
                    <span className="">Do not have an account?</span>
                    <span className="text-red-600 regist"><button id="regisBtn" className="regisBtn">&nbsp;Create account</button></span>
                </div>
                <form class="bg-white rounded px-8 pt-6 pb-8 mb-4">
                    <div class="mb-4 mt-7">
                        <label class="block text-gray-700 text-sm font-bold mb-2" for="email">
                            Email
                        </label>
                        <input
                            class="appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                            id="email"
                            type="email"
                            placeholder="Entry your email"
                        />
                    </div>
                    <div class="mb-6">
                        <label
                            class="block text-gray-700 text-sm font-bold mb-2"
                            for="password"
                        >
                            Password
                        </label>
                        <input
                            class="appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                            id="password"
                            type="password"
                            placeholder="Entry your password"
                        />
                    </div>
                    <div class="flex items-center justify-between">
                    <button
                        class="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        type="button"
                    >
                        Log In
                    </button>
                    </div>
                </form>
            </div>
        </div>
  )
}

export default Login
