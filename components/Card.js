import Image from "next/image";
import Link from "next/link";

const Card = (props) => {
  return (
    <div class="md:w-1/3 rounded overflow-hidden px-5 pt-5 md:pt-0">
      <Image
        class="w-full"
        src={props.src}
        alt="Sunset in the mountains"
      />
      <div class="px-6 py-4">
        <div class="font-bold text-xl mb-2">
          <Link href={`/article/${props.id}`}>{props.title}</Link>
        </div>
        <p class="text-gray-700 text-base">
          {props.description}
        </p>
      </div>
    </div>
  );
};

export default Card;
