import { Html, Head, Main, NextScript } from 'next/document';
import Login from '../components/Login';
import Registration from '../components/Registration';

const Document = () => {
  
  return (
    <Html className='scroll-smooth' style={{scrollBehavior:'smooth'}}>
      <Head />
      <body>
        <Main />
        <Login></Login>
        <Registration></Registration>
        <NextScript />
      </body>
    </Html>
  )
}

export default Document;